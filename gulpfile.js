/**
 * [Dependencias de gulp]
 * @type {[type]}
 */
const 
	gulp 		= require('gulp'),
	concat    	= require('gulp-concat'),
	minifyJS  	= require('gulp-uglify')
;

/**
 * [Tarea que se encarga de minificar los archivos js del proyecto]
 *
 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
 * date 2017-09-13
 * @version [1.0]
 * @param   {[type]} ){	gulp.src('web/public/src/js*.js')	.pipe(concat('main.min.js'))               	.pipe(minifyJS())  	.pipe(gulp.dest('web/public/js'))} [description]
 * @return  {[type]}                                                                    [description]
 */
gulp.task('minify-js', function(){
	gulp.src('web/public/src/js/**/*.js')
	.pipe(concat('main.min.js'))
  	.pipe(minifyJS())
  	.pipe(gulp.dest('web/public/js'))
});

/**
 * [Tarea que se encarga de copilar las tareas (css,js,imagenes,sass, etc.) (Observar'Monitoriar' cambios)]
 *
 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
 * date 2017-09-13
 * @version [1.0]
 * @param   {[type]} ){	gulp.watch('web/public/src/js*.js', ['minify-js']);} [description]
 * @return  {[type]}                                         [description]
 */
gulp.task('watch', function(){
	gulp.watch('web/public/src/js/**/*.js', ['minify-js']);
});

/**
 * Tarea que se encarga de Contruir toda la copilacion en general (Construir gulp)
 * Construir gulp: Termino que le doy para entender que va tomar todos los archivos
 * configurados a las tareas y va volver a generar el o los archivos finales
 */
gulp.task('default',['minify-js']);